package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y){
        // TODO: Implement the logic here

        if ((x == null) || (y == null)) {
            throw new IllegalArgumentException("Null in input parameters. Sequences must be defined.");
        }

        if (x.size() > y.size()) {
            return false;
        }

        if (x.isEmpty()) {
            return true;
        }

        if (y.isEmpty()) {
            return false;
        }
        int nextEqualElementIndex = 0;
        int lastEqualElementIndex = 0;


        for (
                int i = 0; i < x.size(); i++)

        {

            try {
                nextEqualElementIndex = y.subList(lastEqualElementIndex, y.size()).indexOf(x.get(i));
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }

            if (nextEqualElementIndex == -1) {
                return false;
            }

            lastEqualElementIndex = lastEqualElementIndex + nextEqualElementIndex;
        }
        return true;
    }
}

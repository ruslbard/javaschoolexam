package com.tsystems.javaschool.tasks.duplicates;

/**
 * Created by ruslbard on 27.02.2017.
 */
public class ProcessFileElement {
    private Integer duplCounts;
    private Integer sWeight;
    private Integer sLength;
    private String s;

    public Integer getsWeight() {
        return sWeight;
    }

    public Integer getsLength() {
        return sLength;
    }

    public String getS() {
        return s;
    }

    public ProcessFileElement(String processString) {
        this.s = processString;
        this.sLength = s.length();
        this.sWeight = 0;

        for (int i = 0; i < this.sLength; i++) {
            this.sWeight = this.sWeight + s.charAt(i);
        }

        this.duplCounts = 1;
    }

    public Integer getDuplCounts() {
        return this.duplCounts;
    }

    public void incDuplCounts (){
        this.duplCounts++;
    }
}

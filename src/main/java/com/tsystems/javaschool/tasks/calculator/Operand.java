package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by ruslbard on 26.02.2017.
 */
public class Operand extends Lexeme {
    public Operand(String value) {
        super(value);
    }
}

package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by ruslbard on 26.02.2017.
 */
public class Operator extends Lexeme{
    public Operator(String value) {
        super(value);
    }
    public Double eval(Double leftOperand, Double rightOperand){

        Double evalResult;
        switch (this.getValue().charAt(0)){
            case '+':
                evalResult = leftOperand + rightOperand;
                break;
            case '-':
                evalResult = leftOperand - rightOperand;
                break;
            case '*':
                evalResult = leftOperand * rightOperand;
                break;
            case '/':
                if (rightOperand == 0){
                    return null;
                }
                evalResult = leftOperand / rightOperand;
                break;
            default:
                return null;
        }
        return evalResult;
    }
}

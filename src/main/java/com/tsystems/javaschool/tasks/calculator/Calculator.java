package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

        System.out.println("Enter in evaluating method...");

        System.out.println("Check statement for null and minimal length...");
        if ((statement == null) || (statement.length() < 3)) {

            System.out.println("Statement is null or less then minimal length. Return null.");
            return null;
        }
        System.out.println("OK.");

        System.out.println("Input statement is: [" + statement + "]");
        System.out.println("Remove blanks...");
        statement = statement.replace(" ", "");
        System.out.println("OK. Result: [" + statement + "]");

        Stack<String> operatorsStack = new Stack<>();
        ArrayList<Lexeme> lexemesInPostfix = new ArrayList<>();

        System.out.println("Split input statement for lexemes and convert to the postfix with Dijkstra...");

        boolean parseInProgress = true;
        boolean isOperand = false;
        boolean inCase;
        Integer currStatementIndex = 0;
        char currSymbol;

        while (parseInProgress) {

            inCase = true;

            if (currStatementIndex == statement.length()){
                currSymbol = '~';
            }
            else{
                currSymbol = statement.charAt(currStatementIndex);
            }

            while (inCase) {
                switch (currSymbol) {
                    case '+':
                    case '-':
                        isOperand = false;
                        if (operatorsStack.isEmpty()) {
                            operatorsStack.push(statement.substring(currStatementIndex, currStatementIndex + 1));
                            inCase = false;
                            break;
                        }

                        switch (operatorsStack.lastElement().charAt(0)) {
                            case '+':
                            case '-':
                            case '*':
                            case '/':
                                lexemesInPostfix.add(new Operator(operatorsStack.pop()));
                                break;
                            case '(':
                                operatorsStack.push(statement.substring(currStatementIndex, currStatementIndex + 1));
                                inCase = false;
                        }
                        break;

                    case '*':
                    case '/':
                        isOperand = false;
                        if (operatorsStack.empty()) {
                            operatorsStack.push(statement.substring(currStatementIndex, currStatementIndex + 1));
                            inCase = false;
                            break;
                        }

                        switch (operatorsStack.lastElement().charAt(0)) {
                            case '+':
                            case '-':
                            case '(':
                                operatorsStack.push(statement.substring(currStatementIndex, currStatementIndex + 1));
                                inCase = false;
                                break;
                            case '*':
                            case '/':
                                lexemesInPostfix.add(new Operator(operatorsStack.pop()));
                                break;
                        }
                        break;
                    case '(':
                        isOperand = false;
                        operatorsStack.push(statement.substring(currStatementIndex, currStatementIndex + 1));
                        inCase = false;
                        break;
                    case ')':
                        isOperand = false;
                        if (operatorsStack.empty()) {
                            System.out.printf("Wrong input statement. Exit.");
                            return null;
                        }

                        switch (operatorsStack.lastElement().charAt(0)) {
                            case '+':
                            case '-':
                            case '*':
                            case '/':
                                lexemesInPostfix.add(new Operator(operatorsStack.pop()));
                                break;
                            case '(':
                                operatorsStack.pop();
                                inCase = false;

                        }
                        break;
                    case '~':
                        if (operatorsStack.isEmpty()) {
                            parseInProgress = false;
                            inCase = false;
                            break;
                        }
                        switch (operatorsStack.lastElement().charAt(0)){
                            case '+':
                            case '-':
                            case '*':
                            case '/':
                                lexemesInPostfix.add(new Operator(operatorsStack.pop()));
                                break;
                            case '(':
                                System.out.printf("Wrong input statement. Exit.");
                                return null;
                        }
                        break;
                    default:
                        if (isOperand) {
                            Lexeme lastOperandInPostfixStack = lexemesInPostfix.get(lexemesInPostfix.size() - 1);
                            lastOperandInPostfixStack.setValue(lastOperandInPostfixStack.getValue().concat(statement.substring(currStatementIndex, currStatementIndex + 1)));
                        }
                        else {
                            lexemesInPostfix.add(new Operand(statement.substring(currStatementIndex, currStatementIndex + 1)));
                        }
                        isOperand = true;
                        inCase = false;

                }
            }

            if (currStatementIndex < statement.length()){
                currStatementIndex++;
            }
        }

        System.out.println("Done. Result: " + lexemesInPostfix.toString());

        Lexeme currLexeme;
        Stack<Double> evaluateStack = new Stack<>();
        Double leftOperand;
        Double rightOperand;
        Double evalResult;

        System.out.println("Start evaluating...");
        for (int i = 0; i < lexemesInPostfix.size(); i++) {

            try {
                currLexeme = lexemesInPostfix.get(i);

                if (currLexeme instanceof Operand) {
                    evaluateStack.push(Double.valueOf(currLexeme.getValue()));
                }
            }
            catch (Exception e){
                System.out.println(e.getMessage());
                e.printStackTrace();
                return null;
            }

            if (currLexeme instanceof Operator){

                Operator operator = (Operator) currLexeme;
                try{
                    rightOperand = evaluateStack.pop();
                    leftOperand = evaluateStack.pop();

                    evalResult = operator.eval(leftOperand, rightOperand);
                    evaluateStack.push(evalResult);
                }
                catch (Exception e){
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                    return null;
                }
            }
        }
        System.out.println("Done.");

        System.out.println("Try prepare result...");
        String finishEvalValueStr;
        try{
            Double finishEvalValue = evaluateStack.pop();
            DecimalFormat finishResultFormat = new DecimalFormat("0.####");
            finishResultFormat.setGroupingUsed(false);
            finishResultFormat.setDecimalSeparatorAlwaysShown(false);

            finishEvalValueStr = finishResultFormat.format(finishEvalValue).replace(",", ".");

        }
        catch (Exception e){
            return null;
        }

        System.out.println("Finish result is: " + finishEvalValueStr);
        return finishEvalValueStr;
    }
}

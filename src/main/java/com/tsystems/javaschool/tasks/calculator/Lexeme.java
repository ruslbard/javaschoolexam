package com.tsystems.javaschool.tasks.calculator;

/**
 * Created by ruslbard on 26.02.2017.
 */
public abstract class Lexeme {
    private String value;

    public Lexeme(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
